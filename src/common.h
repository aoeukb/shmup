#ifndef COMMON_H
#define COMMON_H
#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdarg.h>

typedef enum {false, true} bool;

#ifdef DEBUG
#define debug_printf(...) printf(__VA_ARGS__)
#else
#define debug_printf(...) NULL
#endif

#endif /* COMMON_H */