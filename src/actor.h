#ifndef SHMUP_ACTOR
#define SHMUP_ACTOR

#include "types.h"

typedef enum s_actor_type
{
    s_actor_type_bullet
} s_actor_type;

typedef struct s_actor
{
    vec2f pos;
    vec2f size;
    s_actor_type type;
} s_actor;

s_actor *s_actor_init(vec2f pos, vec2f size, s_actor_type type);
void s_actor_free(s_actor *ptr);

#endif