#ifndef SHMUP_TYPES
#define SHMUP_TYPES

typedef struct rectf
{
    float left;
    float top;
    float width;
    float height;
} rectf;

typedef struct vec2f
{
    float x;
    float y;
} vec2f;

#endif