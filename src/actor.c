#include "actor.h"
#include <stdlib.h>

s_actor *s_actor_init(vec2f pos, vec2f size, s_actor_type type)
{
    s_actor *ret = malloc(sizeof *ret);
    ret->pos = pos;
    ret->size = size;
    ret->type = type;
    return ret;
}

void s_actor_free(s_actor *ptr)
{
    free(ptr);
}