#include "engine.h"
#include <stdio.h>
#ifdef __linux__
#include <unistd.h>
#endif
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

bool is_running;
SDL_Window *win;
SDL_Renderer *rend;

void do_events(void)
{
	SDL_Event event;
	while(SDL_PollEvent(&event))
	{
        if(event.type == SDL_QUIT)
		{
            is_running = 0;
			break;
		}
    }
}

void engine_loop(void)
{
    while (is_running) 
    {
        do_events();
    }

    SDL_DestroyRenderer(rend);
    SDL_DestroyWindow(win);
    SDL_Quit();
}

int start_engine(char* window_title, char* window_icon)
{
    debug_printf("Starting engine...\n");

    is_running = 1;

    if(SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        printf("Error initializing SDL: %s\n", SDL_GetError());
        return -1;
    }
    else debug_printf("SDL successfully initialized\n");

    win =
        SDL_CreateWindow
        (
            window_title,
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            WINDOW_WIDTH,
            WINDOW_HEIGHT,
            SDL_WINDOW_RESIZABLE
        );
    
    if(!win)
    {
        printf("Failed to create window: %s\n", SDL_GetError());
        return -1;
    }

    if(window_icon != NULL)
    {
        if(access(window_icon, R_OK) == 0)
        {
              SDL_Surface *icon_img = IMG_Load(window_icon);
              SDL_SetWindowIcon(win, icon_img);
              SDL_FreeSurface(icon_img);
        }
        else printf("Icon %s not found\n", window_icon);
    }
    
    rend = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if(!rend)
    {
        printf("Failed to create renderer: %s\n", SDL_GetError());
        return -1;
    }

    engine_loop();

}

int main(int argc, char **argv) 
{
    return start_engine("shmup", NULL);
}