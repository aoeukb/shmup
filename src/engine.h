#ifndef SHMUP_ENGINE
#define SHMUP_ENGINE
#include "common.h"

#define WINDOW_HEIGHT (680)
#define WINDOW_WIDTH (840)

int start_engine(void);

extern bool is_running;
extern SDL_Window *win;
extern SDL_Renderer *rend;

#endif